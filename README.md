# IER SCHOOL
IER School - Students Grades Managements

Installation
------------

### Installing IER SCHOOL application. 

  1. Installing (using git ssh)

    ```bash
    cd /var/www/
    composer global require "fxp/composer-asset-plugin:~1.1.0"
    git clone git@gitlab.com:joepa37/ierschool.dev.git 
    ```

  2. Initialize the installed application

     Execute the `init` command and select `dev` or `prod` as environment.

      ```bash
      cd /var/www/ierschool.dev/
      php init
      ```
  
  3. Configurate your web server:

     For Apache config file could be the following:
     
     ```apacheconf
     <VirtualHost *:80>
       ServerName ierschool.dev
       ServerAlias ierschool.dev
       DocumentRoot "/var/www/ierschool.dev/"
       <Directory "/var/www/ierschool.dev/">
         AllowOverride All
       </Directory>
     </VirtualHost>
     ```
       
  4. Create a new database and adjust the `components['db']` configuration in `common/config/main-local.php` accordingly.

  5. Apply all migrations with console command `php yii migrate --migrationLookup=@common/migrations/,@common/auth/migrations/,@common/settings/migrations/,@common/menu/migrations/,@common/user/migrations/,@common/translation/migrations/,@common/media/migrations/,@common/post/migrations/,@common/page/migrations/,@common/comments/migrations/,@common/comment/migrations/,@common/seo/migrations/`.

  6. Init root user with console command `php yii init-admin`.

  7. Configurate your mailer `['components']['mailer']` in `common/config/main-local.php`.

  8. Modify xdebug max nesting level param adding the following code at the end of php.ini file in apache directory ( C:\wamp\bin\apache\apachex.y.z\bin ):
     
     ```apacheconf
     xdebug.max_nesting_level = 500
     ```

##### Your `IER School` application is installed. Visit your site, the site should work and message _Congratulations! You have successfully created your Yii-powered application_ should be displayed.