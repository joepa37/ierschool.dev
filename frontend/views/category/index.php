<?php

use yii\widgets\LinkPager;

/* @var $this yii\web\View */

$this->title = $category->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">
    <div class="box box-primary">
        <div class="panel-body">
            <?php /* @var $post common\post\models\Post */ ?>
            <?php foreach ($posts as $post) : ?>
                <?= $this->render('/items/post.php', ['post' => $post, 'page' => 'category']) ?>
            <?php endforeach; ?>

            <div class="text-center">
                <?= LinkPager::widget(['pagination' => $pagination]) ?>
            </div>
        </div>
    </div>
</div>
