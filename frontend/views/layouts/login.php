<?php
use frontend\assets\FrontendAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$bundle = FrontendAsset::register($this);

$this->params['body-class'] = array_key_exists('body-class', $this->params) ?
    $this->params['body-class']
    : null;
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?php echo Yii::$app->language ?>">
    <head>
        <meta charset="<?php echo Yii::$app->charset ?>">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <?php echo Html::csrfMetaTags() ?>
        <title><?php echo Html::encode($this->title) ?></title>

        <?php $this->head() ?>
    </head>

    <?php $this->beginBody() ?>
    <body class="hold-transition login-page">
    <?php echo $content ?>
    </body>
    <?php $this->endBody() ?>
    </html>
<?php $this->endPage() ?>