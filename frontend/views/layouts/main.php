<?php
/**
 * @var $this yii\web\View
 */
?>
<?php $this->beginContent('@frontend/views/layouts/common.php'); ?>
<?php echo $content ?>
<?php $this->endContent(); ?>