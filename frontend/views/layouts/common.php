<?php
/**
 * @var $this yii\web\View
 */
use frontend\assets\FrontendAsset;
use common\widgets\SidebarMenu;
use common\auth\assets\AvatarAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\LanguageSelector;
use common\models\Menu;
use common\widgets\Alert;

$bundle = FrontendAsset::register($this);
AvatarAsset::register($this);
?>

<?php $this->beginContent('@frontend/views/layouts/base.php'); ?>
    <div class="wrapper">
        <header class="main-header">
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="<?php echo Yii::$app->urlManager->hostInfo ?>" class="navbar-brand">
                            <div>
                                <?php
                                    $logo = $bundle->baseUrl . '/img/mini-logo.png';
                                    echo Html::img($logo, ['alt' => 'IER School']);
                                ?>
                                <b> IER </b>School
                            </div>
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-chevron-down"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <?php
                        $menuItems = Menu::getMenuItems('main-menu');
                        if (Yii::$app->user->isGuest) {
                            $menuItems[] = ['label' => Yii::t('core/auth', 'Login'), 'url' => ['/auth/default/login']];
                        }
                        echo SidebarMenu::widget([
                            'options'=>['class'=>'nav navbar-nav'],
                            'linkTemplate' => '<a href="{url}"><span>{label}</span>{badge}</a>',
                            'items'=> $menuItems
                        ]); ?>

                        <?php echo LanguageSelector::widget(['display' => 'label', 'view' => 'pills']); ?>
                    </div>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <?php if (!Yii::$app->user->getIsGuest()) : ?>
                                <?php $avatar = ($userAvatar = Yii::$app->user->identity->getAvatar('large')) ? $userAvatar : AvatarAsset::getDefaultAvatar('large'); ?>
                                <li id="log-dropdown" class="dropdown notifications-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-bell"></i>
                                        <span class="label label-success">
                                            <?php echo 10 ?>
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header"><?php echo Yii::t('core', 'You have {num} notification items', ['num'=>10]) ?></li>
                                        <li>
                                            <ul class="menu">

                                            </ul>
                                        </li>
                                        <li class="footer">
                                            <?php echo Html::a(Yii::t('core', 'View all'), ['/notification/index']) ?>
                                        </li>
                                    </ul>
                                </li>

                                <li id="log-dropdown" class="dropdown notifications-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-comment"></i>
                                        <span class="label label-warning">
                                            <?php echo 12 ?>
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header"><?php echo Yii::t('core', 'You have {num} comment items', ['num'=>8]) ?></li>
                                        <li>
                                            <ul class="menu">

                                            </ul>
                                        </li>
                                        <li class="footer">
                                            <?php echo Html::a(Yii::t('core', 'View all'), ['/notification/index']) ?>
                                        </li>
                                    </ul>
                                </li>

                                <li id="log-dropdown" class="dropdown notifications-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-warning"></i>
                                        <span class="label label-danger">
                                            <?php echo 10 ?>
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header"><?php echo Yii::t('core', 'You have {num} log items', ['num'=>10]) ?></li>
                                        <li>
                                            <ul class="menu">

                                            </ul>
                                        </li>
                                        <li class="footer">
                                            <?php echo Html::a(Yii::t('core', 'View all'), ['/log/index']) ?>
                                        </li>
                                    </ul>
                                </li>

                                <li class="dropdown user user-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <img src="<?= $avatar ?>" class="user-image">
                                        <span class="hidden-xs"><?php echo Yii::$app->user->identity->username ?> <i class="caret"></i></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="user-header light-blue">
                                            <img src="<?= $avatar ?>" class="img-circle" alt="User Image" />
                                            <p>
                                                <?php echo Yii::$app->user->identity->username ?>
                                                <small>
                                                    <?php echo Yii::t('core', 'Member since {0, date, short}', Yii::$app->user->identity->created_at) ?>
                                                </small>
                                        </li>
                                        <li class="user-footer">
                                            <div class="pull-left">
                                                <?php echo Html::a(Yii::t('core', 'Profile'), ['/auth/default/profile'], ['class'=>'btn btn-default btn-flat']) ?>
                                            </div>

                                            <?php if(\common\models\User::canRoute(['/admin'])) : ?>
                                                <div class="pull-left">
                                                    <?php echo Html::a(Yii::t('core', 'Administration'), ['/admin'], ['class'=>'btn btn-default btn-flat']) ?>
                                                </div>
                                            <?php endif; ?>

                                            <div class="pull-right">
                                                <?php echo Html::a(Yii::t('core', 'Logout'), ['/auth/default/logout'], ['class'=>'btn btn-default btn-flat', 'data-method' => 'post']) ?>
                                            </div>
                                        </li>
                                    </ul>
                                </li>

                            <?php endif; ?>

                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <div class="content-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?>

                        <?php if (Yii::$app->session->hasFlash('crudMessage')): ?>
                            <div class="alert alert-info alert-dismissible alert-crud" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <?= Yii::$app->session->getFlash('crudMessage') ?>
                            </div>
                        <?php endif; ?>

                        <?php if (Yii::$app->session->hasFlash('success')): ?>
                            <div class="alert alert-info alert-dismissible alert-crud" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <?= Yii::$app->session->getFlash('success') ?>
                            </div>
                        <?php endif; ?>

                        <?php if (Yii::$app->session->hasFlash('error')): ?>
                            <div class="alert alert-error alert-dismissible alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <?= Yii::$app->session->getFlash('error') ?>
                            </div>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="lte-hide-title page-title"><?= Html::encode($this->title) ?></h3>
                                <?php if (isset($this->params['subtitle'])): ?>
                                    <small><?php echo $this->params['subtitle'] ?></small>
                                <?php endif; ?>
                                <?php
                                if(isset($this->params['buttons'])){
                                    foreach ($this->params['buttons'] as $button){
                                        echo $button.' ';
                                    }
                                }
                                if(isset($this->params['alerts'])){
                                    foreach ($this->params['alerts'] as $alert){
                                        echo $alert.' ';
                                    }
                                }
                                ?>

                                <?php if (Yii::$app->session->hasFlash('alert')):?>
                                    <?php echo \yii\bootstrap\Alert::widget([
                                        'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                                        'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
                                    ])?>
                                <?php endif; ?>
                            </div>
                        </div>

                        <?php echo $content ?>

                    </div>
                </div>
            </div>
        </div>

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b> <?php   echo \Yii::t('core', 'Development by'); ?> </b>
                <?php
                    echo \common\Core::powered();
                    echo Yii::t('core', 'Version');
                    echo ' '.Yii::$app->params['version'];
                ?>
            </div>
            <strong>Copyright &copy;  <?php echo date('Y') ?> <a>IER School</a>.</strong>
            <?php  echo Yii::t('core', 'All rights reserved').'.'; ?>
        </footer>

    </div><!-- ./wrapper -->

<?php $this->endContent(); ?>