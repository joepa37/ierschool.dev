<?php

namespace common\post\controllers;

use common\controllers\admin\BaseController;
use common\models\User;

/**
 * PostController implements the CRUD actions for Post model.
 */
class DefaultController extends BaseController
{
    public $modelClass = 'common\post\models\Post';
    public $modelSearchClass = 'common\post\models\search\PostSearch';

    protected function getRedirectPage($action, $model = null)
    {
        if (!User::hasPermission('editPosts') && $action == 'create') {
            return ['view', 'id' => $model->id];
        }

        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }
}