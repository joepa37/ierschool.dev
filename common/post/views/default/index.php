<?php

use common\grid\GridPageSize;
use common\grid\GridQuickLinks;
use common\grid\GridView;
use common\helpers\Html;
use common\models\User;
use common\post\models\Post;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\grid\columns\DateRangePicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\post\models\search\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('core/post', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(Yii::t('core', 'Add New'), ['/post/default/create'], ['class' => 'btn btn-sm btn-primary']),
    Html::a(Yii::t('core/media', 'Categories'), ['/post/category/index'], ['class' => 'btn btn-sm btn-primary'])
];
?>
<div class="post-index">

    <div class="box box-primary">
        <div class="panel-body">

            <?php
            Pjax::begin([
                'id' => 'post-grid-pjax',
            ])
            ?>

            <div class="row">
                <div class="col-sm-6">
                    <?= GridQuickLinks::widget([
                        'model' => Post::className(),
                        'searchModel' => $searchModel,
                        'labels' => [
                            'all' => Yii::t('core', 'All'),
                            'active' => Yii::t('core', 'Published'),
                            'inactive' => Yii::t('core', 'Pending'),
                        ]
                    ]) ?>
                </div>

                <div class="col-sm-6 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'post-grid-pjax']) ?>
                </div>
            </div>

            <?php
                $gridColumn = [
                    ['class' => 'common\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                    [
                        'class' => 'common\grid\columns\TitleActionColumn',
                        'controller' => '/post/default',
                        'title' => function (Post $model) {
                            return Html::a($model->titleTranslation, ['/post/default/view', 'id' => $model->id], ['data-pjax' => 0]);
                        },
                    ],
                    [
                        'attribute' => 'created_by',
                        'label' => Yii::t('core/school', 'Teacher'),
                        'value' => function (Post $model) {
                            return Html::a($model->author->username,
                                ['/user/default/update', 'id' => $model->created_by],
                                ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                        'visible' => User::hasPermission('viewUsers'),
                        'options' => ['style' => 'width:180px'],
                        'filterType' => kartik\grid\GridView::FILTER_SELECT2,
                        'filter' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all(), 'id', 'username'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-class-assign-search-teacher_id']
                    ],
                    [
                        'class' => 'common\grid\columns\StatusColumn',
                        'attribute' => 'status',
                        'optionsArray' => Post::getStatusOptionsList(),
                        'options' => ['style' => 'width:100px'],
                    ],
                    [
                        'attribute' => 'published_at',
                        'value' => function ($model) {
                            return $model->publishedDateTime;
                        },
                        'options' => ['style' => 'width:150px'],
                    ],
                ];
            ?>
            <?=
            GridView::widget([
                'id' => 'post-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'bulkActionOptions' => [
                    'gridId' => 'post-grid',
                    'actions' => [
                        Url::to(['bulk-activate']) => Yii::t('core', 'Publish'),
                        Url::to(['bulk-deactivate']) => Yii::t('core', 'Unpublish'),
                        Url::to(['bulk-delete']) => Yii::t('yii', 'Delete'),
                    ]
                ],
                'columns' => $gridColumn,
                'toolbar' => [
                    '{export}',
                    \kartik\export\ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $gridColumn,
                        'target' => \kartik\export\ExportMenu::TARGET_BLANK,
                        'fontAwesome' => true,
                        'dropdownOptions' => [
                            'label' => 'Full',
                            'class' => 'btn btn-default',
                            'itemsBefore' => [
                                '<li class="dropdown-header">Export All Data</li>',
                            ],
                        ],
                    ]) ,
                ],
            ]);
            ?>

            <?php Pjax::end() ?>
        </div>
    </div>
</div>

<?php
DateRangePicker::widget([
    'model' => $searchModel,
    'attribute' => 'published_at',
])
?>