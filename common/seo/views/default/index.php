<?php

use common\grid\GridPageSize;
use common\grid\GridView;
use common\helpers\Html;
use common\models\User;
use common\seo\models\Seo;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\seo\models\search\SeoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('core/seo', 'Search Engine Optimization');
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(Yii::t('core', 'Add New'), ['/seo/default/create'], ['class' => 'btn btn-sm btn-primary'])
];
?>
<div class="seo-index">
    <div class="box box-primary">
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-12 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'page-grid-pjax']) ?>
                </div>
            </div>

            <?php Pjax::begin(['id' => 'seo-grid-pjax']) ?>

            <?=
            GridView::widget([
                'id' => 'seo-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'bulkActionOptions' => [
                    'gridId' => 'seo-grid',
                    'actions' => [
                        Url::to(['bulk-delete']) => Yii::t('yii', 'Delete'),
                    ]
                ],
                'columns' => [
                    ['class' => 'common\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                    [
                        'attribute' => 'url',
                        'class' => 'common\grid\columns\TitleActionColumn',
                        'controller' => '/seo/default',
                        'title' => function (Seo $model) {
                            return Html::a($model->url, ['/seo/default/update', 'id' => $model->id], ['data-pjax' => 0]);
                        },
                        'buttonsTemplate' => '{update} {delete}',
                    ],
                    'title',
                    //'author',
                    //'keywords',
                    //'description',
                    [
                        'class' => 'common\grid\columns\StatusColumn',
                        'attribute' => 'index',
                        'options' => ['style' => 'width:30px'],
                    ],
                    [
                        'class' => 'common\grid\columns\StatusColumn',
                        'attribute' => 'follow',
                        'options' => ['style' => 'width:30px'],
                    ],
                ],
            ]);
            ?>

            <?php Pjax::end() ?>
        </div>
    </div>
</div>


