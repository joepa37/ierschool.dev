<?php
/* @var $this yii\web\View */
?>

<div class="box box-solid" style="border-radius: 10px;">
    <div class="box-header">
        <i class="fa fa-cube"></i>

        <h3 class="box-title">
            <?= Yii::t('core', 'IER School') ?>
        </h3>
    </div>
    <div class="box-body" style="padding: 0">
        <?php
            $bundle = \backend\assets\BackendAsset::register($this);
            $logo = $bundle->baseUrl . '/img/IER School.png';
            $options = [
                'class' => 'img-responsive pad',
                'style' => 'padding: 0;',
                'alt' => 'IER School'
            ];
            echo \common\helpers\Html::img($logo, $options);
        ?>
    </div>
</div>