<?php

use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\auth\models\forms\RegistrationForm $model
 */
$this->title = Yii::t('core/auth', 'Signup');

$col12 = $this->context->module->gridColumns;
$col9 = (int) ($col12 * 3 / 4);
$col6 = (int) ($col12 / 2);
$col3 = (int) ($col12 / 4);
?>


<?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert-alert-warning text-center">
        <?= Yii::$app->session->getFlash('error') ?>
    </div>
<?php endif; ?>

<div class="login-box">
    <div class="login-logo text-center">
        <a class="login-logo" href="<?php echo Yii::$app->urlManager->hostInfo ?>" style="link, visited {text-decoration: none; } hover {text-decoration: none; }">
            <?php
            $logo = \frontend\assets\FrontendAsset::register($this)->baseUrl . '/img/logo.png';
            echo Html::img($logo, ['alt' => 'IER School']);
            ?>
            <b> IER </b>School
        </a>
    </div>
    <div class="login-box-body">
        <b>
            <p class="login-box-msg">Sigup</p>
        </b>

            <?php $form = ActiveForm::begin([
                'id' => 'signup',
                'validateOnBlur' => true,
                'options' => ['autocomplete' => 'off'],
                'fieldConfig' => [
                    'template' => "{input}\n{error}",
                ],
            ]); ?>

            <div class="form-group has-feedback">
                <?= $form->field($model, 'username')->textInput(['placeholder' => $model->getAttributeLabel('username'), 'autocomplete' => 'off', 'maxlength' => 50]) ?>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'email')->textInput(['placeholder' => $model->getAttributeLabel('email'), 'autocomplete' => 'off', 'maxlength' => 255]) ?>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'autocomplete' => 'off', 'maxlength' => 255]) ?>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'repeat_password')->passwordInput(['placeholder' => $model->getAttributeLabel('repeat_password'), 'autocomplete' => 'off', 'maxlength' => 255]) ?>
                <span class="glyphicon glyphicon-repeat form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <p class="help-block">Clic on image to reload captcha.</p>
                <?= $form->field($model, 'captcha')->widget(Captcha::className(), [
                    'options' => [
                        'placeholder' => 'Captcha',
                        'class' => 'form-control',
                    ],
                    'template' => '<div class="row"><div class="col-sm-' . $col6 . '">{image}</div><div class="col-sm-' . $col6 . '">{input}</div></div>',
                    'captchaAction' => [\yii\helpers\Url::to(['/auth/captcha'])]
                ]) ?>
            </div>

            <div class="form-group has-feedback">
                <?= Html::submitButton(Yii::t('core/auth', 'Signup'), ['class' => 'btn btn-primary btn-block btn-flat']) ?>
            </div>

            <div class="row registration-block">
                <div class="col-sm-<?= $col6 ?>">
                    <?= Html::a(Yii::t('core/auth', "Login"), ['default/login']) ?>
                </div>
                <div class="col-sm-<?= $col6 ?> text-right">
                    <?= Html::a(Yii::t('core/auth', "Forgot password?"), ['default/reset-password']) ?>
                </div>
            </div>

        <?php ActiveForm::end() ?>
    </div>
</div>

<?php
