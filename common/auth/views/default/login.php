<?php

/**
 * @var $this yii\web\View
 * @var $model common\auth\models\forms\LoginForm
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$this->title = Yii::t('core/auth', 'Authorization');

$col12 = $this->context->module->gridColumns;
$col9 = (int) ($col12 * 3 / 4);
$col6 = (int) ($col12 / 2);
$col3 = (int) ($col12 / 4);
?>


    <div class="login-box">
       <div class="login-logo text-center">
            <a class="login-logo" href="<?php echo Yii::$app->urlManager->hostInfo ?>">
                <?php
                    $logo = \frontend\assets\FrontendAsset::register($this)->baseUrl . '/img/logo.png';
                    echo Html::img($logo, ['alt' => 'IER School']);
                ?>
                <b> IER </b>School
            </a>
        </div>
        <div class="login-box-body">
            <b>
                <p class="login-box-msg"><?= Yii::t('core/user', 'Sign in to start your session')?></p>
            </b>

            <?php
            $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['autocomplete' => 'off'],
                'validateOnBlur' => true,
                'fieldConfig' => [
                    'template' => "{input}\n{error}",
                ],
            ])
            ?>

            <div class="form-group has-feedback">
                <?= $form->field($model, 'username')->textInput(['autofocus' => true,'placeholder' => $model->getAttributeLabel('username'), 'autocomplete' => 'off']) ?>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'autocomplete' => 'off']) ?>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <?= $form->field($model, 'rememberMe')->checkbox(['value' => true]) ?>
            </div>

            <div class="form-group has-feedback">
                <?= Html::submitButton(Yii::t('core/auth', 'Login'), ['class' => 'btn btn-primary btn-block btn-flat']) ?>
            </div>

            <div class="row registration-block">
                <div class="col-sm-<?= $col6 ?> ">
                    <?= Html::a(Yii::t('core/auth', "Forgot password?"), ['default/reset-password']) ?>
                </div>
            </div>
            </div>

            <?php ActiveForm::end() ?>

        </div>
    </div>

<?php