<?php

namespace common\auth\assets;

use yii\web\AssetBundle;

/**
 * AuthAsset is an asset bundle for [[common\auth\widgets\AuthChoice]] widget.
 */
class AuthAsset extends AssetBundle
{
    public $sourcePath = '@common/auth/assets/source';
    public $css = [
        'authstyle.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
