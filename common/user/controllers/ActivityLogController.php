<?php

namespace common\user\controllers;

use common\controllers\admin\BaseController;

class ActivityLogController extends BaseController
{
    /**
     *
     * @inheritdoc
     */
    public $modelClass = 'common\models\UserActivityLog';

    /**
     *
     * @inheritdoc
     */
    public $modelSearchClass = 'common\user\models\search\UserActivityLogSearch';

    /**
     *
     * @inheritdoc
     */
    public $enableOnlyActions = ['index', 'view', 'grid-page-size'];
}
