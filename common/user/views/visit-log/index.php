<?php

use common\grid\columns\DateRangePicker\DateRangePicker;
use common\grid\GridPageSize;
use common\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\user\models\search\UserVisitLogSearch $searchModel
 */

$this->title = Yii::t('core/user', 'Visit Log');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/user', 'Users'), 'url' => ['/user/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="user-visit-log-index">

        <div class="box box-primary">
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-12 text-right">
                        <?= GridPageSize::widget(['pjaxId' => 'user-visit-log-grid-pjax']) ?>
                    </div>
                </div>

                <?php
                Pjax::begin([
                    'id' => 'user-visit-log-grid-pjax',
                ])
                ?>

                <?=
                GridView::widget([
                    'id' => 'user-visit-log-grid',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'bulkActions' => ' ',
                    'columns' => [
                        [
                            'attribute' => 'user_id',
                            'class' => 'common\grid\columns\TitleActionColumn',
                            'controller' => '/user/visit-log',
                            'title' => function ($model) {
                                return Html::a($model->user->username, ['/user/default/update', 'id' => $model->user->id], ['data-pjax' => 0]);
                            },
                            'buttonsTemplate' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a(Yii::t('core/user', 'View Visit Log'),
                                        Url::to(['visit-log/view', 'id' => $model->id]), [
                                            'title' => Yii::t('core/user', 'View Visit Log'),
                                            'data-pjax' => '0'
                                        ]
                                    );
                                },
                            ]
                        ],
                        'language',
                        'os',
                        'browser',
                        array(
                            'attribute' => 'ip',
                            'value' => function ($model) {
                                return Html::a($model->ip,
                                    "http://ipinfo.io/" . $model->ip,
                                    ["target" => "_blank"]);
                            },
                            'format' => 'raw',
                        ),
                        [
                            'attribute' => 'visit_time',
                            'value' => function ($model) {
                                return $model->visitDatetime;
                            },
                        ],
                        /* [
                          'attribute' => 'author_id',
                          'filter' => common\models\User::getUsersList(),
                          'value' => function(Post $model) {
                          return Html::a($model->author->username,
                          ['user/view', 'id' => $model->author_id],
                          ['data-pjax' => 0]);
                          },
                          'format' => 'raw',
                          'options' => ['style' => 'width:180px'],
                          ],
                          [
                          'class' => 'common\grid\columns\StatusColumn',
                          'attribute' => 'status',
                          'optionsArray' => Post::getStatusOptionsList(),
                          'options' => ['style' => 'width:60px'],
                          ],
                          [
                          'class' => 'common\grid\columns\DateFilterColumn',
                          'attribute' => 'published_at',
                          'value' => function(Post $model) {
                          return '<span style="font-size:85%;" class="label label-'
                          .((time() >= $model->published_at) ? 'primary' : 'default').'">'
                          .$model->publishedDate.'</span>';
                          },
                          'format' => 'raw',
                          'options' => ['style' => 'width:150px'],
                          ], */
                    ],
                ]);
                ?>

                <?php Pjax::end() ?>
            </div>
        </div>
    </div>

<?php
DateRangePicker::widget([
'model' => $searchModel,
'attribute' => 'visit_time',
])
?>