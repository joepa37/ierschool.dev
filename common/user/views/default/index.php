<?php

use common\grid\GridPageSize;
use common\grid\GridQuickLinks;
use common\grid\GridView;
use common\helpers\Html;
use common\models\Role;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\user\models\search\UserSearch $searchModel
 */
$this->title = Yii::t('core/user', 'Users');
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(Yii::t('core', 'Add New'), ['/user/default/create'], ['class' => 'btn btn-sm btn-primary'])
];
?>
<div class="user-index">
    <div class="box box-primary">
        <div class="panel-body">

            <?php
            Pjax::begin([
                'id' => 'user-grid-pjax',
            ])
            ?>

            <div class="row">
                <div class="col-sm-6">
                    <?= GridQuickLinks::widget([
                        'model' => User::className(),
                        'searchModel' => $searchModel,
                        'options' => [
                            ['label' => Yii::t('core', 'All'), 'filterWhere' => []],
                            ['label' => Yii::t('core', 'Active'), 'filterWhere' => ['status' => User::STATUS_ACTIVE]],
                            ['label' => Yii::t('core', 'Inactive'), 'filterWhere' => ['status' => User::STATUS_INACTIVE]],
                            ['label' => Yii::t('core', 'Banned'), 'filterWhere' => ['status' => User::STATUS_BANNED]],
                        ]
                    ]) ?>
                </div>

                <div class="col-sm-6 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'user-grid-pjax']) ?>
                </div>
            </div>



            <?php
                //todo, add banned action to bulkActionsOptions
            ?>

            <?= GridView::widget([
                'id' => 'user-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'bulkActionOptions' => [
                    'gridId' => 'user-grid',
                ],
                'columns' => [
                    ['class' => 'common\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                    [
                        'attribute' => 'username',
                        'controller' => '/user/default',
                        'class' => 'common\grid\columns\TitleActionColumn',
                        'title' => function (User $model) {
                            if (User::hasPermission('editUsers')) {
                                return Html::a($model->username, ['/user/default/update', 'id' => $model->id], ['data-pjax' => 0]);
                            } else {
                                return $model->username;
                            }
                        },
                        'buttonsTemplate' => '{update} {delete} {permissions} {password}',
                        'buttons' => [
                            'permissions' => function ($url, $model, $key) {
                                return Html::a(Yii::t('core/user', 'Permissions'),
                                    Url::to(['user-permission/set', 'id' => $model->id]), [
                                        'title' => Yii::t('core/user', 'Permissions'),
                                        'data-pjax' => '0'
                                    ]
                                );
                            },
                            'password' => function ($url, $model, $key) {
                                return Html::a(Yii::t('core/user', 'Password'),
                                    Url::to(['default/change-password', 'id' => $model->id]), [
                                        'title' => Yii::t('core/user', 'Password'),
                                        'data-pjax' => '0'
                                    ]
                                );
                            }
                        ],
                        'options' => ['style' => 'width:300px']
                    ],
                    [
                        'attribute' => 'email',
                        'format' => 'raw',
                        'visible' => User::hasPermission('viewUserEmail'),
                    ],
                    /* [
                      'class' => 'common\grid\columns\StatusColumn',
                      'attribute' => 'email_confirmed',
                      'visible' => User::hasPermission('viewUserEmail'),
                      ], */
                    [
                        'attribute' => 'gridRoleSearch',
                        'filter' => ArrayHelper::map(Role::getAvailableRoles(Yii::$app->user->isSuperAdmin),
                            'name', 'description'),
                        'value' => function (User $model) {
                            return implode(', ',
                                ArrayHelper::map($model->roles, 'name',
                                    'description'));
                        },
                        'format' => 'raw',
                        'visible' => User::hasPermission('viewUserRoles'),
                    ],
                    /*  [
                      'attribute' => 'registration_ip',
                      'value' => function(User $model) {
                      return Html::a($model->registration_ip,
                      "http://ipinfo.io/".$model->registration_ip,
                      ["target" => "_blank"]);
                      },
                      'format' => 'raw',
                      'visible' => User::hasPermission('viewRegistrationIp'),
                      ], */
                    [
                        'class' => 'common\grid\columns\StatusColumn',
                        'attribute' => 'superadmin',
                        'visible' => Yii::$app->user->isSuperadmin,
                    ],
                    [
                        'class' => 'common\grid\columns\StatusColumn',
                        'attribute' => 'status',
                        'optionsArray' => [
                            [User::STATUS_ACTIVE, Yii::t('core', 'Active'), 'primary'],
                            [User::STATUS_INACTIVE, Yii::t('core', 'Inactive'), 'info'],
                            [User::STATUS_BANNED, Yii::t('core', 'Banned'), 'default'],
                        ],
                    ],
                ],
            ]);
            ?>

            <?php Pjax::end() ?>

        </div>
    </div>
</div>
