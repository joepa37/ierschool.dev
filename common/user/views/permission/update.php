<?php
/**
 * @var common\widgets\ActiveForm $form
 * @var common\models\Permission $model
 */

use yii\helpers\Html;

$this->title = Yii::t('core/user', 'Update Permission');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/user', 'Users'), 'url' => ['/user/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/user', 'Permissions'), 'url' => ['/user/permission/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="permission-update">
    <?= $this->render('_form', compact('model')) ?>
</div>