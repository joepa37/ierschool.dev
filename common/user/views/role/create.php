<?php
/**
 *
 * @var yii\web\View $this
 * @var common\widgets\ActiveForm $form
 * @var common\models\Role $model
 */
use yii\helpers\Html;

$this->title = Yii::t('core/user', 'Create Role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/user', 'Users'), 'url' => ['/user/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/user', 'Roles'), 'url' => ['/user/role/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-create">
    <?= $this->render('_form', compact('model')) ?>
</div>