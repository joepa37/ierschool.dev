<?php
/**
 * @var $this yii\web\View
 */
use backend\assets\BackendAsset;
use common\widgets\SidebarMenu;
use common\auth\assets\AvatarAsset;
use yii\helpers\ArrayHelper;
use common\helpers\Html;
use common\widgets\LanguageSelector;
use common\models\Menu;
use common\widgets\Breadcrumbs;

$bundle = BackendAsset::register($this);
AvatarAsset::register($this);
?>

<?php $this->beginContent('@common/views/layouts/admin/base.php'); ?>

    <div class="wrapper" id="wrapper" style="height: auto;">
        <header class="main-header">
            <a href="<?php echo Yii::$app->urlManager->hostInfo.'/admin' ?>" class="logo">
                <?php $logo = $bundle->baseUrl . '/img/mini-logo.png'; ?>
                <span class="logo-mini">
                    <?= Html::img($logo, ['alt' => 'IER School']); ?>
                </span>
                <span class="logo-lg">
                    <?= Html::img($logo, ['alt' => 'IER School', 'style' => 'margin-right: 10px;']); ?>
                    <b>IER </b>School
                </span>
            </a>
            <nav class="navbar navbar-static-top">
                <a href="/admin" class="sidebar-toggle" data-toggle="offcanvas" role="button" onclick="saveState()">
                    <span class="sr-only"><?php echo Yii::t('core', 'Toggle navigation') ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <i class="fa fa-chevron-down"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                    <?php
                    $menuItems = Menu::getMenuItems('backend-menu');
                    echo SidebarMenu::widget([
                        'options'=>['class'=>'nav navbar-nav'],
                        'linkTemplate' => '<a href="{url}"><span>{label}</span>{badge}</a>',
                        'items'=> $menuItems
                    ]); ?>

                    <?php echo LanguageSelector::widget(['display' => 'label', 'view' => 'pills']); ?>
                </div>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <?php $avatar = ($userAvatar = Yii::$app->user->identity->getAvatar('large')) ? $userAvatar : AvatarAsset::getDefaultAvatar('large') ?>
                        <li id="log-dropdown" class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell"></i><span class="label label-success"><?php echo 10 ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header"><?php echo Yii::t('core', 'You have {num} notification items', ['num'=>10]) ?></li>
                                <li>
                                    <ul class="menu">
                                    </ul>
                                </li>
                                <li class="footer">
                                    <?php echo Html::a(Yii::t('core', 'View all'), ['/notification/index']) ?>
                                </li>
                            </ul>
                        </li>
                        <li id="log-dropdown" class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-comment"></i>
                            <span class="label label-warning"><?php echo 12 ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header"><?php echo Yii::t('core', 'You have {num} comment items', ['num'=>8]) ?></li>
                                <li>
                                    <ul class="menu">
                                    </ul>
                                </li>
                                <li class="footer">
                                    <?php echo Html::a(Yii::t('core', 'View all'), ['/notification/index']) ?>
                                </li>
                            </ul>
                        </li>
                        <li id="log-dropdown" class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-warning"></i>
                            <span class="label label-danger"><?php echo 10 ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header"><?php echo Yii::t('core', 'You have {num} log items', ['num'=>10]) ?></li>
                                <li>
                                    <ul class="menu">
                                    </ul>
                                </li>
                                <li class="footer">
                                    <?php echo Html::a(Yii::t('core', 'View all'), ['/log/index']) ?>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= $avatar ?>" class="user-image">
                                <span class="hidden-xs"><?php echo Yii::$app->user->identity->username ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header light-blue">
                                    <img src="<?= $avatar ?>" class="img-circle" alt="User Image" />
                                    <p>
                                        <?php echo Yii::$app->user->identity->username ?>
                                        <small>
                                            <?php echo Yii::t('core', 'Member since {0, date, short}', Yii::$app->user->identity->created_at) ?>
                                        </small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a class="btn btn-default btn-flat" href="<?php echo Yii::$app->urlManager->hostInfo . '/auth/default/profile'?>"><?php echo Yii::t('core', 'Profile') ?></a>
                                    </div>
                                    <div class="pull-left">
                                        <a class="btn btn-default btn-flat" href="<?php echo Yii::$app->urlManager->hostInfo?>"><?php echo Yii::t('core', 'Homepage') ?></a>
                                    </div>
                                    <div class="pull-right">
                                        <a class="btn btn-default btn-flat" data-method="post" href="<?php echo Yii::$app->urlManager->hostInfo . '/auth/logout'?>"><?php echo Yii::t('core', 'Logout') ?></a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar" style="height: auto;">
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?= $avatar ?>" class="img-circle" />
                    </div>
                    <div class="pull-left info">
                        <p><?php echo Yii::t('core', 'Hello, {username}', ['username'=>Yii::$app->user->identity->username]) ?></p>
                        <a href="<?php echo Yii::$app->urlManager->hostInfo . '/auth/default/profile'?>">
                            <i class="fa fa-circle text-success"></i>
                            <?php echo Yii::$app->formatter->asDate(time()) ?>
                        </a>
                    </div>
                </div>
                <?php echo SidebarMenu::widget([
                    'options'=>['class'=>'sidebar-menu'],
                    'linkTemplate' => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                    'submenuTemplate'=>"\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                    'activateParents'=>true,
                    'items'=> Menu::getMenuItems('admin-menu')
                ]) ?>
            </section>
        </aside>
        <div class="content-wrapper" style="min-height: 1126px;">
            <section class="content-header">
                <h1>
                    <?= Html::encode($this->title) ?>
                    <?php if (isset($this->params['subtitle'])): ?>
                        <small><?php echo $this->params['subtitle'] ?></small>
                    <?php endif; ?>
                    <?php
                    if(isset($this->params['buttons'])){
                        foreach ($this->params['buttons'] as $button){
                            echo $button.' ';
                        }
                    }
                    ?>
                </h1>

                <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?>

            </section>
            <section class="content">
                <?php echo $content ?>
            </section>
        </div>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <strong>
                    <?php
                        echo \Yii::t('core', 'Development by');
                        echo \common\Core::powered();
                    ?>
                </strong>
                <?php
                    echo Yii::t('core', 'Version');
                    echo ' '.Yii::$app->params['version'];
                ?>
            </div>
            <strong>Copyright &copy;  <?php echo date('Y') ?> <a>IER School</a>.</strong>
            <?php  echo Yii::t('core', 'All rights reserved').'.'; ?>
        </footer>
    </div>

<?php $this->endContent(); ?>
