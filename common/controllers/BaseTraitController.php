<?php

namespace common\controllers;

use common\helpers\CoreHelper;
use Yii;

trait BaseTraitController
{
    public $writeLog = true;
    public $logParam1, $logParam2, $logParam3, $logParam4, $logExtraParams = NULL;

    public function afterAction($action, $result)
    {
        $this->saveLog();
        return parent::afterAction($action, $result);
    }
    
    public function saveLog(){
        if($this->writeLog){
            $browser = new \Ikimea\Browser\Browser();

            $log = new \common\models\UserActivityLog();
            $log->user_id = Yii::$app->user->isGuest ? 0 : Yii::$app->user->id;
            $log->ipaddress = CoreHelper::getRealIp();
            $log->controller = Yii::$app->id === $this->module->id ? $this->id : $this->module->id.'/'.$this->id;
            $log->action = $this->action->id;
            $log->param1 = isset($this->logParam1) ? $this->logParam1 : null ;
            $log->param2 = isset($this->logParam2) ? $this->logParam2 : null ;
            $log->param3 = isset($this->logParam3) ? $this->logParam3 : null ;
            $log->param4 = isset($this->logParam4) ? $this->logParam4 : null ;
            $log->extra_params = isset($this->logExtraParams) ? $this->logExtraParams : null ;
            $log->os = $browser->getPlatform();
            $log->browser = $browser->getBrowser();
            $log->browser_version = $browser->getVersion();
            $log->user_agent = $browser->getUserAgent();
            $log->save(false);
        }
    }
}
