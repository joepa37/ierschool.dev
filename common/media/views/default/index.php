<?php

use common\assets\LanguagePillsAsset;
use common\media\assets\ModalAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = Yii::t('core/media', 'Media');
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(Yii::t('core/media', 'Manage Albums'), ['/media/album/index'], ['class' => 'btn btn-sm btn-primary'])
];

ModalAsset::register($this);
LanguagePillsAsset::register($this);

?>

<div class="media-index">
    <?= common\media\widgets\Gallery::widget() ?>
</div>

