<?php

use common\grid\GridPageSize;
use common\grid\GridView;
use common\helpers\Html;
use common\media\models\Category;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\media\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('core/media', 'Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Media'), 'url' => ['/media/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Albums'), 'url' => ['/media/album/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(Yii::t('core', 'Add New'), ['/media/category/create'], ['class' => 'btn btn-sm btn-primary']),
    Html::a(Yii::t('core/media', 'Manage Albums'), ['/media/album/index'], ['class' => 'btn btn-sm btn-primary'])
];

?>
<div class="media-category-index">

    <div class="box box-primary">
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-12 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'media-category-grid-pjax']) ?>
                </div>
            </div>

            <?php Pjax::begin(['id' => 'media-category-grid-pjax']) ?>

            <?= GridView::widget([
                'id' => 'media-category-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'bulkActionOptions' => [
                    'gridId' => 'media-category-grid',
                    'actions' => [Url::to(['bulk-delete']) => Yii::t('core', 'Delete')]
                ],
                'columns' => [
                    ['class' => 'common\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                    [
                        'class' => 'common\grid\columns\TitleActionColumn',
                        'controller' => '/media/category',
                        'title' => function (Category $model) {
                            return Html::a($model->title, ['/media/category/update', 'id' => $model->id], ['data-pjax' => 0]);
                        },
                        'buttonsTemplate' => '{update} {delete}',
                    ],
                    'description:ntext',
                    [
                        'class' => 'common\grid\columns\StatusColumn',
                        'attribute' => 'visible',
                    ],
                ],
            ]); ?>

            <?php Pjax::end() ?>
        </div>
    </div>
</div>