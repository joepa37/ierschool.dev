<?php

use common\helpers\Html;
use yii\widgets\ListView;
use common\comments\models\Comment;

/**
 * https://plus.google.com/+joepa37
 * @copyright Copyright (c) 2016 José Peña
 * @license http://www.apache.org/licenses/LICENSE-2.0
 *
 * Created by PhpStorm.
 * User: José Peña
 * Date: 27/2/2017
 * Time: 9:31 AM
 */

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '<ul class="mailbox-attachments clearfix">{items}</ul><div class="text-center">{pager}</div>',
    'itemOptions' => ['tag' => 'li','class' => 'item'],
    'itemView' => function ($model, $key, $index, $widget) {
        $img = $model->getThumbImage('small', ['url' => $this->params['moduleBundle']->baseUrl, 'alt' => $model->alt]);
        $originalURL = $model->getThumbUrl('original');
        $comments =
            Html::a(
                '<i class="fa fa-comments"></i> ' .
                Yii::t('core', 'Comments {number}', ['number' => '('. Comment::activeCount(\common\media\models\Media::className(), $model->id) .')']),
                ['/media/default/comment', 'id' => $model->id],
                ['class' => 'btn btn-default btn-xs pull-right', 'style' => 'margin-right: 1px;', 'target' => '_blank']
            );
        $footer = '<span class="mailbox-attachment-size">'.$model->getFileSize().'
                        <a href='.$originalURL.' download='.$model->filename.' class="btn btn-default btn-xs pull-right" style="margin-right: -5px;">
                            <i class="fa fa-cloud-download"></i>
                        </a>
                         '.$comments.'
                    </span>';

        if($model->isImage()){
            $item = '
                <span class="mailbox-attachment-icon has-img">
                    '.$img.'
                </span>
                <div class="mailbox-attachment-info">
                    <a target="_blank"  href='.$originalURL.' class="mailbox-attachment-name users-list-name">
                        <i class="fa fa-camera"></i> '.$model->title.'
                    </a>
                    '.$footer.'
                </div>';
        }else{
            $fa = $model->getFA_Array();
            $item = '
                <span class="mailbox-attachment-icon">
                    <i class="'.$fa['name'].' fa-lg" style="color:'.$fa['color'].';"></i>
                </span>
                <div class="mailbox-attachment-info">
                    <a target="_blank"  href='.$originalURL.' class="mailbox-attachment-name users-list-name">
                        <i class="fa fa-paperclip"></i> '.$model->title.'
                    </a>
                    '.$footer.'
                </div>';
        }

        return Html::a($item, '#mediafile', ['data-key' => $key]);

    },
    'pager' => [
        'options' => [
            'class' => 'pagination pagination-sm',
            'style' => 'display: inline-block;',
        ],
        'hideOnSinglePage' => true,
        'firstPageLabel' => '<<',
        'prevPageLabel' => '<',
        'nextPageLabel' => '>',
        'lastPageLabel' => '>>',
    ],
]);
