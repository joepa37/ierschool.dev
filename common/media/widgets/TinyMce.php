<?php

namespace common\media\widgets;

use pendalf89\tinymce\TinyMce as TinyMceWidget;
use common\media\assets\FileInputAsset;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\InputWidget;

class TinyMce extends InputWidget
{
    //TODO media tools available only for small, medium and large size, for insert display all

    /**
     * @var string Optional, if set, only this image can be selected by user
     */
    public $thumb = '';

    /**
     * @var string JavaScript function, which will be called before insert file data to input.
     * Argument data contains file data.
     * data example: [alt: "Witch with cat", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
     */
    public $callbackBeforeInsert = '';

    /**
     * @var array the options for the TinyMCE JS plugin.
     * Please refer to the TinyMCE JS plugin Web page for possible options.
     * @see http://www.tinymce.com/wiki.php/Configuration
     */

    public $clientOptions = [
        'menubar' => true,
        'height' => 600,
        'image_dimensions' => true,
        'image_caption' => true,
        'plugins' => [
            'advlist autolink lists link image charmap print preview hr anchor
            searchreplace visualblocks visualchars code  contextmenu table wordcount
            pagebreak insertdatetime save media nonbreaking paste directionality emoticons template
            textcolor fullpage colorpicker textpattern codesample toc',
        ],

        'toolbar1' =>    'undo redo | bold italic underline strikethrough | forecolor backcolor emoticons | alignleft aligncenter alignright alignjustify bullist numlist outdent indent',
        'toolbar2' =>    'print preview | styleselect formatselect fontselect fontsizeselect | pagebreak link image table | code codesample',
        'toolbar_items_size' => 'small',
    ];

    /**
     * @var string TinyMCE widget
     */
    private $tinyMCE = '';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (empty($this->clientOptions['file_picker_callback'])) {
            $this->clientOptions['file_picker_callback'] = new JsExpression(
                'function(callback, value, meta) {
                    mediaTinyMCE(callback, value, meta);
                }'
            );
        }

        if (empty($this->clientOptions['document_base_url'])) {
            $this->clientOptions['document_base_url'] = '';
        }

        if (empty($this->clientOptions['convert_urls'])) {
            $this->clientOptions['convert_urls'] = false;
        }

        if (empty($this->clientOptions['language'])) {
            if( \Yii::$app->language != 'en' ){
                $this->clientOptions['language'] = \Yii::$app->language;
            }
        }

        $this->tinyMCE = TinyMceWidget::widget([
            'name' => $this->name,
            'model' => $this->model,
            'attribute' => $this->attribute,
            'clientOptions' => $this->clientOptions,
            'options' => $this->options,
        ]);
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        FileInputAsset::register($this->view);

        if (!empty($this->callbackBeforeInsert)) {
            $this->view->registerJs('
                $("#' . $this->options['id'] . '").on("fileInsert", ' . $this->callbackBeforeInsert . ');'
            );
        }

        $modal = $this->renderFile('@common/media/views/manage/modal.php',
            [
                'inputId' => $this->options['id'],
                'btnId' => $this->options['id'] . '-btn',
                'frameId' => $this->options['id'] . '-frame',
                'frameSrc' => Url::to(['/media/manage']),
                'thumb' => $this->thumb,
            ]);

        $this->registerCSS();

        return $this->tinyMCE . $modal;
    }

    public function registerCSS(){
        //TODO change post-content_ifr title for tooltip
        $css = <<<CSS
            body .ui-tooltip {
            display: none !important;
        }
CSS;
        $this->view->registerCss($css);
    }
}