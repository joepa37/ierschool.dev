<?php

namespace common\media\widgets\dashboard;

use common\media\models\Media as MediaModel;
use common\models\User;
use common\widgets\DashboardWidget;
use yii\data\ActiveDataProvider;

class Media extends DashboardWidget
{
    /**
     * Most recent post limit
     */
    public $recentLimit = 8;

    /**
     * Post index action
     */
    public $indexAction = 'media/default/index';

    public function run()
    {
        if (User::hasPermission('viewMedia')) {
            $recent = MediaModel::find()->orderBy(['id' => SORT_DESC])->limit($this->recentLimit);
            $dataProvider = new ActiveDataProvider([
                'query' => $recent,
                'pagination' => false
            ]);

            return $this->render('media',
                [
                    'height' => $this->height,
                    'width' => $this->width,
                    'position' => $this->position,
                    'recent' => $recent,
                    'dataProvider' => $dataProvider,
                ]);
        }
    }
}