<?php

namespace common\media\widgets;

use common\helpers\CoreHelper;
use common\models\OwnerAccess;
use common\models\User;
use Yii;
use yii\helpers\StringHelper;

class Gallery extends \yii\base\Widget
{
    /**
     * @var ActiveRecord
     */
    public $modelClass = 'common\media\models\Media';

    /**
     * @var ActiveRecord
     */
    public $modelSearchClass = 'common\media\models\MediaSearch';
    public $pageSize = 24;
    public $pageSizeModal = 16;
    public $mode = 'normal';

    public function run()
    {
        $modelClass = $this->modelClass;
        $searchModel = $this->modelSearchClass ? new $this->modelSearchClass : null;

        $restrictAccess = (CoreHelper::isImplemented($modelClass, OwnerAccess::CLASSNAME)
            && !User::hasPermission($modelClass::getFullAccessPermission()));

        $searchName = StringHelper::basename($searchModel::className());
        $params = Yii::$app->request->getQueryParams();

        if ($restrictAccess) {
            $params[$searchName][$modelClass::getOwnerField()] = Yii::$app->user->identity->id;
        }

        $dataProvider = $searchModel->search($params);
        if($this->mode == 'modal'){
            $dataProvider->pagination->pageSize = $this->pageSizeModal;
        }else{
            $dataProvider->pagination->pageSize = $this->pageSize;
        }

        $this->registerCSS();

        return $this->render('gallery', [
                'searchModel' => $searchModel,
                'mode' => $this->mode,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    public function registerCSS(){
        $css = <<< CSS
            .pagination {
                margin-top: 8px;
                margin-bottom: 0;
            }
CSS;
        $this->getView()->registerCss($css);
    }
}