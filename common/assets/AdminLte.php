<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets;

use yii\web\AssetBundle;

class AdminLte extends AssetBundle
{
    public $sourcePath = '@common/assets/admin-lte';
    public $js = [
        'js/app.min.js'
    ];
    public $css = [
        'css/AdminLTE.min.css',
        'css/skins/_all-skins.css',
    ];
    public $depends = [
        //'yii\web\JqueryAsset',
        'yii\jui\JuiAsset', //required by sortable on dashboard
        //todo, donwload fonts and include to the proyect by localy
        'yii\bootstrap\BootstrapPluginAsset', //(required by admin-lte)
        'common\assets\FontAwesome', //required by the FA icons
        'common\assets\JquerySlimScroll' //(required by the secctions like notifications are)
    ];
}
