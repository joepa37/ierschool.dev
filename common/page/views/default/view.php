<?php

use common\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\page\models\Page */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/page', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(Yii::t('core', 'Add New'), ['/page/default/create'], ['class' => 'btn btn-sm btn-primary']),
    Html::a(Yii::t('core', 'Edit'), ['/page/default/update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']),
    Html::a(Yii::t('core', 'Delete'), ['/page/default/delete', 'id' => $model->id], [
        'class' => 'btn btn-sm btn-warning pull-right',
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ])
];
?>
<div class="page-view">
    <div class="box box-primary">
        <div class="panel-body">
            <h2><?= $model->title ?></h2>
            <?= $model->content ?>
        </div>
    </div>
</div>
