<?php

namespace common\models;

use Yii;
use common\db\ActiveRecord;

/**
 * This is the model class for table "user_activity_log".
 *
 * @property integer $user_id
 * @property string $ipaddress
 * @property string $logtime
 * @property string $controller
 * @property string $action
 * @property string $param1
 * @property string $param2
 * @property string $param3
 * @property string $param4
 * @property string $extra_params
 * @property string $os
 * @property string $browser
 * @property string $browser_version
 * @property string $user_agent
 */
class UserActivityLog extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_activity_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ipaddress', 'controller', 'action', 'os', 'browser', 'browser_version', 'user_agent', 'logtime'], 'required'],
            [['user_id'], 'integer'],
            [['extra_params'], 'string'],
            [['ipaddress', 'controller'], 'string', 'max' => 50],
            [['action', 'browser'], 'string', 'max' => 30],
            [['param1', 'param2', 'param3', 'param4'], 'string', 'max' => 255],
            [['os'], 'string', 'max' => 20],
            [['browser_version'], 'string', 'max' => 10],
            [['user_agent'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('core', 'User'),
            'ipaddress' => Yii::t('core', 'IP'),
            'logtime' => Yii::t('core', 'Visit Time'),
            'controller' => Yii::t('core', 'Controller'),
            'action' => Yii::t('core', 'Action'),
            'param1' => Yii::t('core', 'Parameter 1'),
            'param2' => Yii::t('core', 'Parameter 2'),
            'param3' => Yii::t('core', 'Parameter 3'),
            'param4' => Yii::t('core', 'Parameter 4'),
            'extra_params' => Yii::t('core', 'Additional Parameter'),
            'os' => Yii::t('core', 'OS'),
            'browser' => Yii::t('core', 'Browser'),
            'browser_version' => Yii::t('core', 'Browser Version'),
            'user_agent' => Yii::t('core', 'User agent'),
        ];
    }
    
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Get visit date
     *
     * @return string
     */
    public function getLogDate()
    {
        return Yii::$app->formatter->asDate($this->logtime);
    }

    /**
     * Get visit time
     *
     * @return string
     */
    public function getLogTime()
    {
        return Yii::$app->formatter->asTime($this->logtime);
    }

    /**
     * Get visit datetime
     *
     * @return string
     */
    public function getLogDatetime()
    {
        return "{$this->logDate} {$this->logTime}";
    }

}
