<?php

use common\comments\Comments;
use common\comments\widgets\CommentsForm;
use yii\helpers\ArrayHelper;
use common\helpers\Html;
use yii\timeago\TimeAgo;

?>
<div class="user-block">
    <?php if (Comments::getInstance()->displayAvatar): ?>
        <img class="img-circle img-bordered-sm" src="<?= Comments::getInstance()->renderUserAvatar($model->user_id) ?>" alt="user image">
    <?php endif; ?>
    <span class="username">
      <a><?= Html::encode($model->user->fullName); ?></a>
    </span>
    <span class="info">
        <p><?= Html::encode($model->content); ?></p>
    </span>
    <span class="description">
        <?php if ($nested_level < Comments::getInstance()->maxNestedLevel): ?>
            <?php if (!Comments::getInstance()->onlyRegistered || !Yii::$app->user->isGuest): ?>
                <a href="#" class="reply-button" data-reply-to="<?= $model->id; ?>">
                    <?= Comments::t('comments', 'Reply') ?>
                </a>
            <?php endif; ?>
        <?php endif; ?>

        <?php if ($nested_level < Comments::getInstance()->maxNestedLevel): ?>
            <span style="margin-left: 5px; margin-right: 5px;">  •  </span>
        <?php endif; ?>
        <span>
            <?php if(date('Ymd') == date('Ymd', $model->created_at)) : ?>
                <?= Comments::t('comments', 'Shared'); ?>
                <?= TimeAgo::widget(['timestamp' => $model->created_at]); ?>
             <?php else : ?>
                 <?= $model->createdDateTime; ?>
            <?php endif; ?>
        </span>
    </span>
</div>

<?php if ($nested_level < Comments::getInstance()->maxNestedLevel): ?>
    <?php if (!Comments::getInstance()->onlyRegistered || !Yii::$app->user->isGuest): ?>
        <div class="reply-form<?= (Comments::getInstance()->displayAvatar) ? ' display-avatar' : '' ?>">
            <?php if ($model->id == ArrayHelper::getValue(Yii::$app->getRequest()->post(), 'Comment.parent_id')) : ?>
                <?= CommentsForm::widget(['reply_to' => $model->id]); ?>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (!empty($model->comments)) : ?>
        <div class="sub-comments box-footer box-comments">
            <?php $nested_level++; ?>
            <?php foreach ($model->comments as $model) : ?>
                <div class="comment box-comment">
                    <?= $this->render('comment', compact('model', 'widget', 'nested_level')) ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php endif; ?>
