<?php

namespace common\settings\controllers;

use common\controllers\admin\BaseController;
use common\helpers\CoreHelper;
use Yii;

/**
 * CacheController implements Flush Cache page.
 *
 * @author José Peña <joepa37@gmail.com>
 */
class CacheController extends BaseController
{
    /**
     * @inheritdoc
     */
    public $enableOnlyActions = ['flush'];

    public function actionFlush()
    {
        $frontendAssetPath = Yii::getAlias('@frontend') . '/web/assets/';
        $backendAssetPath = Yii::getAlias('@backend') . '/web/assets/';

        CoreHelper::recursiveDelete($frontendAssetPath);
        CoreHelper::recursiveDelete($backendAssetPath);

        if (!is_dir($frontendAssetPath)) {
            @mkdir($frontendAssetPath);
        }

        if (!is_dir($backendAssetPath)) {
            @mkdir($backendAssetPath);
        }

        if (Yii::$app->cache->flush()) {
            Yii::$app->session->setFlash('crudMessage', 'Cache has been flushed.');
        } else {
            Yii::$app->session->setFlash('crudMessage', 'Failed to flush cache.');
        }

        return Yii::$app->getResponse()->redirect(Yii::$app->getRequest()->referrer);
    }
}