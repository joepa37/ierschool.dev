<?php

namespace common\settings\controllers;

/**
 * ReadingController implements Reading Settings page.
 *
 * @author José Peña <joepa37@gmail.com>
 */
class ReadingController extends SettingsBaseController
{
    public $modelClass = 'common\settings\models\ReadingSettings';
    public $viewPath = '@common/settings/views/reading/index';

}