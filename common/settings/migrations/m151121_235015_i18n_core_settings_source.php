<?php

use common\db\SourceMessagesMigration;

class m151121_235015_i18n_core_settings_source extends SourceMessagesMigration
{

    public function getCategory()
    {
        return 'core/settings';
    }

    public function getMessages()
    {
        return [
            'General Settings' => 1,
            'Reading Settings' => 1,
            'Site Title' => 1,
            'Site Description' => 1,
            'Admin Email' => 1,
            'Timezone' => 1,
            'Date Format' => 1,
            'Time Format' => 1,
            'Save Date Format' => 1,
            'Display Date Format' => 1,
            'Page Size' => 1,
        ];
    }
}