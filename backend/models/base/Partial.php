<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\OwnerAccess;

/**
 * This is the base model class for table "partial".
 *
 * @property integer $id
 * @property string $name
 * @property integer $curricular_malla_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \backend\models\GradeDetailPartial[] $gradeDetailPartials
 * @property \common\models\User $createdBy
 * @property \backend\models\CurricularMalla $curricularMalla
 * @property \common\models\User $updatedBy
 */
class Partial extends \common\db\ActiveRecord implements OwnerAccess
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partial}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'curricular_malla_id'], 'required'],
            [['curricular_malla_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('core/school', 'ID'),
            'name' => \Yii::t('core/school', 'Name'),
            'curricular_malla_id' => \Yii::t('core/school', 'Curricular Malla ID'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradeDetailPartials()
    {
        return $this->hasMany(\backend\models\GradeDetailPartial::className(), ['partial_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurricularMalla()
    {
        return $this->hasOne(\backend\models\CurricularMalla::className(), ['id' => 'curricular_malla_id']);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\PartialQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\queries\PartialQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullPartialAccess';
    }
}
