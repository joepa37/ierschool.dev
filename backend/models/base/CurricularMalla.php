<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\OwnerAccess;

/**
 * This is the base model class for table "curricular_malla".
 *
 * @property integer $id
 * @property string $name
 * @property integer $modality_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \backend\models\ClassTable[] $classTables
 * @property \backend\models\Course[] $courses
 * @property \common\models\User $createdBy
 * @property \backend\models\Modality $modality
 * @property \common\models\User $updatedBy
 * @property \backend\models\Partial[] $partials
 */
class CurricularMalla extends \common\db\ActiveRecord implements OwnerAccess
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%curricular_malla}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'modality_id'], 'required'],
            [['modality_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('core/school', 'ID'),
            'name' => \Yii::t('core/school', 'Name'),
            'modality_id' => \Yii::t('core/school', 'Modality ID'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassTables()
    {
        return $this->hasMany(\backend\models\ClassTable::className(), ['curricular_malla_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(\backend\models\Course::className(), ['curricular_malla_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModality()
    {
        return $this->hasOne(\backend\models\Modality::className(), ['id' => 'modality_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartials()
    {
        return $this->hasMany(\backend\models\Partial::className(), ['curricular_malla_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\CurricularMallaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\queries\CurricularMallaQuery(get_called_class());
    }

    /**
     *
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullCurricularMallaAccess';
    }
}
