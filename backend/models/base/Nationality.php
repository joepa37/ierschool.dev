<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\OwnerAccess;

/**
 * This is the base model class for table "nationality".
 *
 * @property integer $id
 * @property string $name
 *
 * @property \backend\models\ParentOrTutor[] $parentOrTutors
 * @property \backend\models\Student[] $students
 */
class Nationality extends \common\db\ActiveRecord implements OwnerAccess
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%nationality}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('core/school', 'ID'),
            'name' => \Yii::t('core/school', 'Name'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentOrTutors()
    {
        return $this->hasMany(\backend\models\ParentOrTutor::className(), ['nationality_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(\backend\models\Student::className(), ['nationality_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\NationalityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\queries\NationalityQuery(get_called_class());
    }

    /**
     *
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullNationalityAccess';
    }
}
