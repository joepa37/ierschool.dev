<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "parent_or_tutor".
 *
 * @property integer $id
 * @property string $name
 * @property string $dni
 * @property integer $nationality_id
 * @property integer $gender_id
 * @property integer $birth_day
 * @property integer $birth_month
 * @property integer $birth_year
 * @property string $pob
 * @property integer $county_id
 * @property string $address
 * @property string $phone_number
 * @property string $email
 * @property string $occupation
 * @property string $workplace
 * @property string $workplace_phone_number
 * @property string $avatar
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \backend\models\County $county
 * @property \backend\models\User $createdBy
 * @property \backend\models\Nationality $nationality
 * @property \backend\models\User $updatedBy
 * @property \backend\models\Student[] $students
 */
class ParentOrTutor extends \common\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'dni', 'nationality_id', 'gender_id', 'county_id'], 'required'],
            [['nationality_id', 'gender_id', 'birth_day', 'birth_month', 'birth_year', 'county_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['avatar'], 'string'],
            [['name', 'email'], 'string', 'max' => 255],
            [['dni'], 'string', 'max' => 15],
            [['pob', 'phone_number', 'occupation', 'workplace', 'workplace_phone_number'], 'string', 'max' => 45],
            [['address'], 'string', 'max' => 70]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parent_or_tutor';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('core/school', 'ID'),
            'name' => Yii::t('core/school', 'Name'),
            'dni' => Yii::t('core/school', 'Dni'),
            'nationality_id' => Yii::t('core/school', 'Nationality ID'),
            'gender_id' => Yii::t('core/school', 'Gender ID'),
            'birth_day' => Yii::t('core/school', 'Birth Day'),
            'birth_month' => Yii::t('core/school', 'Birth Month'),
            'birth_year' => Yii::t('core/school', 'Birth Year'),
            'pob' => Yii::t('core/school', 'Pob'),
            'county_id' => Yii::t('core/school', 'County ID'),
            'address' => Yii::t('core/school', 'Address'),
            'phone_number' => Yii::t('core/school', 'Phone Number'),
            'email' => Yii::t('core/school', 'Email'),
            'occupation' => Yii::t('core/school', 'Occupation'),
            'workplace' => Yii::t('core/school', 'Workplace'),
            'workplace_phone_number' => Yii::t('core/school', 'Workplace Phone Number'),
            'avatar' => Yii::t('core/school', 'Avatar'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounty()
    {
        return $this->hasOne(\backend\models\County::className(), ['id' => 'county_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\backend\models\User::className(), ['id' => 'created_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNationality()
    {
        return $this->hasOne(\backend\models\Nationality::className(), ['id' => 'nationality_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\backend\models\User::className(), ['id' => 'updated_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(\backend\models\Student::className(), ['tutor_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\ParentOrTutorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\queries\ParentOrTutorQuery(get_called_class());
    }
}
