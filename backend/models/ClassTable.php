<?php

namespace backend\models;

use \backend\models\base\ClassTable as BaseClassTable;

/**
 * This is the model class for table "class_table".
 */
class ClassTable extends BaseClassTable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'curricular_malla_id'], 'required'],
            [['curricular_malla_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 70]
        ]);
    }
}
