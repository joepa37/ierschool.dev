<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Course;

/**
 * backend\models\search\CourseSearch represents the model behind the search form about `backend\models\Course`.
 */
 class CourseSearch extends Course
{
     public $modality_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_period_id', 'modality_id', 'curricular_malla_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Course::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->request->cookies->getValue('_grid_page_size', 20),
            ],
            'sort'=>[
                'defaultOrder'=>['id'=> SORT_DESC],
            ],
        ]);

        $query->joinWith('curricularMalla.modality');

        $dataProvider->sort->attributes['modality_id'] = [
            'asc' => ['modality.name' => SORT_ASC],
            'desc' => ['modality.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'school_period_id' => $this->school_period_id,
            'modality.id' => $this->modality_id,
            'curricular_malla_id' => $this->curricular_malla_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'course.name', $this->name]);

        return $dataProvider;
    }
}
