<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\GradeDetailPartial;

/**
 * backend\models\search\GradeDetailPartialSearch represents the model behind the search form about `backend\models\GradeDetailPartial`.
 */
 class GradeDetailPartialSearch extends GradeDetailPartial
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'grade_detail_id', 'partial_id', 'acumulative_1', 'acumulative_2', 'exam', 'leveling', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GradeDetailPartial::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'grade_detail_id' => $this->grade_detail_id,
            'partial_id' => $this->partial_id,
            'acumulative_1' => $this->acumulative_1,
            'acumulative_2' => $this->acumulative_2,
            'exam' => $this->exam,
            'leveling' => $this->leveling,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
