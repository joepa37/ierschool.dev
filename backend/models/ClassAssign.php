<?php

namespace backend\models;

use \backend\models\base\ClassAssign as BaseClassAssign;

/**
 * This is the model class for table "class_assign".
 */
class ClassAssign extends BaseClassAssign
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['teacher_id', 'section_id', 'class_table_id'], 'required'],
            [['teacher_id', 'section_id', 'class_table_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer']
        ]);
    }
}
