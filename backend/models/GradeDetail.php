<?php

namespace backend\models;

use \backend\models\base\GradeDetail as BaseGradeDetail;

/**
 * This is the model class for table "grade_detail".
 */
class GradeDetail extends BaseGradeDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['grade_id', 'student_id'], 'required'],
            [['grade_id', 'student_id', 'retrieval', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer']
        ]);
    }
}
