<?php

namespace backend\models;

use \backend\models\base\Grade as BaseGrade;

/**
 * This is the model class for table "grade".
 */
class Grade extends BaseGrade
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['section_id', 'date_range'], 'required'],
            [['section_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['date_range'], 'string', 'max' => 33]
        ]);
    }
}
