<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Student */

?>
<div class="student-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->name) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'dni',
        'status',
        [
            'attribute' => 'nationality.name',
            'label' => Yii::t('core/school', 'Nationality'),
        ],
        'gender_id',
        'birth_day',
        'birth_month',
        'birth_year',
        'pob',
        [
            'attribute' => 'county.name',
            'label' => Yii::t('core/school', 'County'),
        ],
        'address',
        'phone_number',
        'email:email',
        [
            'attribute' => 'father.name',
            'label' => Yii::t('core/school', 'Father'),
        ],
        [
            'attribute' => 'mother.name',
            'label' => Yii::t('core/school', 'Mother'),
        ],
        [
            'attribute' => 'tutor.name',
            'label' => Yii::t('core/school', 'Tutor'),
        ],
        'observations',
        'avatar:ntext',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>