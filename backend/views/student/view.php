<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Student */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Student'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= Yii::t('core/school', 'Student').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . Yii::t('core/school', 'PDF'), 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('core/school', 'Will open the generated PDF file in a new window')
                ]
            )?>
            <?= Html::a(Yii::t('core/school', 'Save As New'), ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a(Yii::t('core/school', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('core/school', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('core/school', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'dni',
        'status',
        [
            'attribute' => 'nationality.name',
            'label' => Yii::t('core/school', 'Nationality'),
        ],
        'gender_id',
        'birth_day',
        'birth_month',
        'birth_year',
        'pob',
        [
            'attribute' => 'county.name',
            'label' => Yii::t('core/school', 'County'),
        ],
        'address',
        'phone_number',
        'email:email',
        [
            'attribute' => 'father.name',
            'label' => Yii::t('core/school', 'Father'),
        ],
        [
            'attribute' => 'mother.name',
            'label' => Yii::t('core/school', 'Mother'),
        ],
        [
            'attribute' => 'tutor.name',
            'label' => Yii::t('core/school', 'Tutor'),
        ],
        'observations',
        'avatar:ntext',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerGradeDetail->totalCount){
    $gridColumnGradeDetail = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'grade.id',
                'label' => Yii::t('core/school', 'Grade')
            ],
                        'retrieval',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerGradeDetail,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-grade-detail']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('core/school', 'Grade Detail')),
        ],
        'columns' => $gridColumnGradeDetail
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerStudentAssign->totalCount){
    $gridColumnStudentAssign = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'section.name',
                'label' => Yii::t('core/school', 'Section')
            ],
                ];
    echo Gridview::widget([
        'dataProvider' => $providerStudentAssign,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-student-assign']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('core/school', 'Student Assign')),
        ],
        'columns' => $gridColumnStudentAssign
    ]);
}
?>
    </div>
</div>
