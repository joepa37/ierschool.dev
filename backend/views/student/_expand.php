<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('core/school', 'Student')),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('core/school', 'Grade Detail')),
        'content' => $this->render('_dataGradeDetail', [
            'model' => $model,
            'row' => $model->gradeDetails,
        ]),
    ],
                                        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('core/school', 'Student Assign')),
        'content' => $this->render('_dataStudentAssign', [
            'model' => $model,
            'row' => $model->studentAssigns,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
