<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ClassTable */

$this->title = Yii::t('core/school', 'Update {modelClass}: ', [
    'modelClass' => 'Class Table',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Class Tables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('core/school', 'Update');
?>
<div class="class-table-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
