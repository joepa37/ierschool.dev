<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\County */

$this->title = Yii::t('core/school', 'Update {modelClass}: ', [
    'modelClass' => 'County',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'County'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('core/school', 'Update');
?>
<div class="county-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
