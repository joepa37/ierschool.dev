<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\County */

$this->title = Yii::t('core/school', 'Create County');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'County'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="county-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
