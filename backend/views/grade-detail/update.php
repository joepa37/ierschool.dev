<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\GradeDetail */

$this->title = Yii::t('core/school', 'Update {modelClass}: ', [
    'modelClass' => 'Grade Detail',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Grade Details'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('core/school', 'Update');
?>
<div class="grade-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
