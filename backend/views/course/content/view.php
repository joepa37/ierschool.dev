<?php
/**
 * https://plus.google.com/+joepa37
 * @copyright Copyright (c) 2016 José Peña
 * @license http://www.apache.org/licenses/LICENSE-2.0
 *
 * Created by PhpStorm.
 * User: José Peña
 * Date: 17/1/2017
 * Time: 4:27 PM
 */

/* @var $model backend\models\Course */

?>

<div class="course-content">
    <?php
    if($model->getSections()->count()>0){
        echo $this->render('section', [
            'model' => $model,
        ]);
    }
    ?>
    <?php
    if($model->curricularMalla->getClassTables()->count()>0){
        foreach($model->curricularMalla->getClassTables()->all() as $class){
            var_dump($class);
            echo $this->render('class', [
                'model' => $class,
            ]);
        }
    }
    ?>
</div>

<?php
$js = <<< JS
    function resizeWidthOnly(a,b) {
        var c = [window.innerWidth];
            return onresize = function() {
                var d = window.innerWidth,
                e = c.length;
                c.push(d);
                if(c[e]!==c[e-1]){
                    clearTimeout(b);
                    b = setTimeout(a, 0);
                }
        }, a;
    }

    resizeWidthOnly(function() {
        resize();
    });

    $( document ).ready(function() {
        setTimeout( function(){ $(".content-sections").fadeIn( "slow" ); }, 0);
        setTimeout( function(){ resize(); }, 0);
    });

    $(window).resize(function() {
        setTimeout( function(){ resize(); }, 0);
    });

    function resize(){
        var width = $(".content-sections").width();

        if( width > 0 && width < 780 ){
            $(".content-section").removeClass().addClass("content-section col-sm-12");
        }

        if( width > 780 ){
            $(".content-section").removeClass().addClass("content-section col-sm-6");
        }
    }
JS;

$this->registerJs($js,  yii\web\View::POS_END);