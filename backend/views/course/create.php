<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Course */

$this->title = Yii::t('core/school', 'Create');
$this->params['subtitle'] = Yii::t('core/school', 'Course');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'School Periods'), 'url' => ['/school-period/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Courses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('<i class="fa fa-list"></i> ' . Yii::t('core/School', 'Courses'), ['/course/index'], ['class' => common\Core::LIST_CLASS]),
];
?>
<div class="course-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
