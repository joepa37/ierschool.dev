<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\ClassAssign */

?>
<div class="class-assign-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'teacher.username',
            'label' => Yii::t('core/school', 'Teacher'),
        ],
        [
            'attribute' => 'section.name',
            'label' => Yii::t('core/school', 'Section'),
        ],
        [
            'attribute' => 'classTable.name',
            'label' => Yii::t('core/school', 'Class Table'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>