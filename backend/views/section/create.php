<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Section */

$this->title = Yii::t('core', 'Create');
$this->params['subtitle'] = Yii::t('core/school', 'Section');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'School Periods'), 'url' => ['/school-period/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Courses'), 'url' => ['/course/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Sections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('<i class="fa fa-list"></i> ' . Yii::t('core/School', 'Sections'), ['/section/index'], ['class' => common\Core::LIST_CLASS]),
];
?>
<div class="section-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
