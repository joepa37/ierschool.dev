<?php

use common\helpers\Html;
use common\widgets\ActiveForm;
use common\widgets\ActiveField;
use kartik\touchspin\TouchSpin;

/* @var $this yii\web\View */
/* @var $model backend\models\Section */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="section-form">

    <?php
    $form = ActiveForm::begin([
        'id' => 'section-form',
    ])
    ?>

    <div class="row">
        <div class="col-md-9">

            <div class="box box-primary">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-sm-6">

                            <?= $form->field($model, 'name',['hintType' => ActiveField::HINT_SPECIAL])
                                ->widget(TouchSpin::classname(),
                                    [
                                        'options' => ['placeholder' => Yii::t('core', 'Select...')],
                                        'pluginOptions' => [
                                            'prefix' => Yii::t('core/school', 'Section'),
                                            'initval' => 1,
                                            'min' => 1,
                                            'max' => 10,
                                        ]
                                    ]);
                            ?>

                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'journey_id', ['hintType' => \common\widgets\ActiveField::HINT_SPECIAL])->widget(\kartik\widgets\Select2::classname(), [
                                'data' => \yii\helpers\ArrayHelper::map(\backend\models\Journey::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                                'options' => ['placeholder' => Yii::t('core', 'Select...')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => Html::icon('book')
                                    ],
                                ]
                            ]); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field(
                                isset($model->course->schoolPeriod) ? $model->course->schoolPeriod : $model,
                                isset($model->course->schoolPeriod) ? 'id' : 'school_period_id',
                                ['hintType' => \common\widgets\ActiveField::HINT_SPECIAL])
                                ->widget(\kartik\widgets\Select2::classname(), [
                                    'data' => \yii\helpers\ArrayHelper::map(\backend\models\SchoolPeriod::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                                    'options' => [
                                        'id'=>'school_period_id',
                                        'placeholder' => Yii::t('core', 'Select...')
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ],
                                    'addon' => [
                                        'prepend' => [
                                            'content' => Html::icon('bookmark')
                                        ],
                                    ]
                                ]); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'course_id', ['hintType' => \common\widgets\ActiveField::HINT_SPECIAL])
                                ->widget(\kartik\depdrop\DepDrop::className(), [
                                    'data' => [
                                        isset($model->course) ? $model->course->id : null =>
                                            isset($model->course) ? $model->course->name : null
                                    ],
                                    'type'=>\kartik\depdrop\DepDrop::TYPE_SELECT2,
                                    'options' => [
                                        'id'=>'course_id',
                                        'placeholder' => Yii::t('core', 'Select...'),
                                    ],
                                    'select2Options'=>[
                                        'pluginOptions'=>['allowClear'=>true],
                                        'addon' => [
                                            'prepend' => [
                                                'content' => Html::icon('arrow-left')
                                            ],
                                        ]
                                    ],
                                    'pluginOptions' => [
                                        'initialize' =>isset($model->course) ? true : false,
                                        'depends'=>['school_period_id'],
                                        'url'=>\yii\helpers\Url::to(['/section/courses']),
                                        'loadingText' => Yii::t('core', 'Loading...'),
                                    ],
                            ]); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-3">

            <div class="box box-success">
                <div class="panel-body">
                    <div class="record-info">

                        <?php if (!$model->isNewRecord): ?>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['created_by'] ?> :
                                </label>
                                <span><?= $model->createdBy->username ?></span>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['updated_by'] ?> :
                                </label>
                                <span><?= $model->updatedBy->username ?></span>
                            </div>

                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['created_at'] ?> :
                                </label>
                                <span><?= "{$model->createdDate} {$model->createdTime}" ?></span>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['updated_at'] ?> :
                                </label>
                                <span><?= $model->updatedDatetime ?></span>
                            </div>
                            <div class="divider"></div>
                        <?php else: ?>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['created_by'] ?> :
                                </label>
                                <span><?= Yii::$app->user->identity->username ?></span>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['created_at'] ?> :
                                </label>
                                <span><?= "{$model->createdDate} {$model->createdTime}" ?></span>
                            </div>
                            <div class="divider"></div>
                        <?php endif; ?>

                        <div class="form-group">
                            <?php if ($model->isNewRecord): ?>
                                <?= Html::submitButton(Yii::t('core', 'Create'), ['class' => common\Core::CREATE_CLASS]) ?>
                                <?= Html::a(Yii::t('core', 'Cancel'), ['/section/index'], ['class' => common\Core::CANCEL_CLASS]) ?>
                            <?php else: ?>
                                <?= Html::submitButton(Yii::t('core', 'Save'), ['class' => common\Core::SAVE_CLASS]) ?>
                                <?= Html::a(Yii::t('core', 'Delete'), ['/section/delete', 'id' => $model->id], [
                                    'class' => common\Core::DELETE_CLASS,
                                    'data' => [
                                        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]) ?>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
