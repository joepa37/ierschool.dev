<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Grade */

$this->title = Yii::t('core/school', 'Save As New {modelClass}: ', [
    'modelClass' => 'Grade',
]). ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Grades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('core/school', 'Save As New');
?>
<div class="grade-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
