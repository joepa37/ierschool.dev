<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Grade */

$this->title = Yii::t('core/school', 'Create Grade');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Grades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grade-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
