<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\State */

$this->title = Yii::t('core/school', 'Create State');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'State'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="state-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
