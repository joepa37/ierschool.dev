<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\GradeDetailPartial */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Grade Detail Partials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grade-detail-partial-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('core/school', 'Grade Detail Partial').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'gradeDetail.id',
                'label' => Yii::t('core/school', 'Grade Detail')
            ],
        [
                'attribute' => 'partial.name',
                'label' => Yii::t('core/school', 'Partial')
            ],
        'acumulative_1',
        'acumulative_2',
        'exam',
        'leveling',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
