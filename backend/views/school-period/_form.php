<?php

use common\helpers\Html;
use common\widgets\ActiveForm;
use common\widgets\ActiveField;
//use kartik\daterange\DateRangePicker;
use common\grid\datepicker\DateRangePicker;
use kartik\touchspin\TouchSpin;

/* @var $this yii\web\View */
/* @var $model backend\models\SchoolPeriod */
/* @var $form common\widgets\ActiveForm */

?>

<div class="school-period-form">

    <?php
    $form = ActiveForm::begin([
        'id' => 'school-period-form',
    ])
    ?>

    <div class="row">
        <div class="col-md-9">

            <div class="box box-primary">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-sm-6">

                            <?= $form->field($model, 'name',['hintType' => ActiveField::HINT_SPECIAL])
                                ->widget(TouchSpin::classname(),
                                [
                                    'options' => ['placeholder' => Yii::t('core', 'Select...')],
                                    'pluginOptions' => [
                                        'prefix' => Yii::t('core/school', 'School Period'),
                                        'initval' => $model->getActualYear(),
                                        'min' => $model->getMinYear(),
                                        'max' => $model->getMaxYear(),
                                    ]
                                ]);
                            ?>

                        </div>
                        <div class="col-sm-6">
                            <?=  $form->field($model, 'date_range', [
                                'hintType' => ActiveField::HINT_SPECIAL,
                                'options'=>['class'=>'drp-container form-group']
                            ])
                            ->widget(DateRangePicker::classname(), [
                                'model'=>$model,
                                'attribute' => 'date_range',
                                'startAttribute' => 'start_at',
                                'endAttribute' => 'end_at',
                                'hideInput'=>true,
                            ]);?>
                        </div>
                    </div>

                </div>
            </div>

            <?php if (!$model->isNewRecord): ?>
                <?= $this->render('content/view', [
                    'model' => $model,
                ]) ?>
            <?php endif; ?>

        </div>

        <div class="col-md-3">

            <div class="box box-success">
                <div class="panel-body">
                    <div class="record-info">

                        <?php if (!$model->isNewRecord): ?>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['created_by'] ?> :
                                </label>
                                <span><?= $model->createdBy->username ?></span>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['updated_by'] ?> :
                                </label>
                                <span><?= $model->updatedBy->username ?></span>
                            </div>

                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['created_at'] ?> :
                                </label>
                                <span><?= "{$model->createdDate} {$model->createdTime}" ?></span>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['updated_at'] ?> :
                                </label>
                                <span><?= $model->updatedDatetime ?></span>
                            </div>
                            <div class="divider"></div>
                        <?php else: ?>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['created_by'] ?> :
                                </label>
                                <span><?= Yii::$app->user->identity->username ?></span>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['created_at'] ?> :
                                </label>
                                <span><?= "{$model->createdDate} {$model->createdTime}" ?></span>
                            </div>
                            <div class="divider"></div>
                        <?php endif; ?>

                        <div class="form-group">
                            <?php if ($model->isNewRecord): ?>
                                <?= Html::submitButton(Yii::t('core', 'Create'), ['class' => common\Core::CREATE_CLASS]) ?>
                                <?= Html::a(Yii::t('core', 'Cancel'), ['/school-period/index'], ['class' => common\Core::CANCEL_CLASS]) ?>
                            <?php else: ?>
                                <?= Html::submitButton(Yii::t('core', 'Save'), ['class' => common\Core::SAVE_CLASS]) ?>
                                <?= Html::a(Yii::t('core', 'Delete'), ['/school-period/delete', 'id' => $model->id], [
                                    'class' => common\Core::DELETE_CLASS,
                                    'data' => [
                                        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]) ?>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
