<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SchoolPeriod */

$this->title = Yii::t('core', 'Create');
$this->params['subtitle'] = Yii::t('core/school', 'School Period');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'School Periods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('<i class="fa fa-list"></i> ' . Yii::t('core/School', 'School Periods'), ['/school-period/index'], ['class' => common\Core::LIST_CLASS]),
];
?>
<div class="school-period-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
