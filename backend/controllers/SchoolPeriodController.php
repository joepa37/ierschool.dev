<?php

namespace backend\controllers;

use Yii;
use common\controllers\admin\BaseController;

/**
 * SchoolPeriodController implements the CRUD actions for SchoolPeriod model.
 */
class SchoolPeriodController extends BaseController
{
    public $modelClass = 'backend\models\SchoolPeriod';
    public $modelSearchClass = 'backend\models\search\SchoolPeriodSearch';
    public $disabledActions = ['view', 'bulk-activate', 'bulk-deactivate'];

    protected function getRedirectPage($action, $model = null)
    {
        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /* @var $model \common\db\ActiveRecord */
        $model = new $this->modelClass;

        $model->date_range = $model->getDateRange();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('crudMessage', Yii::t('core','Your item has been created.'));
            return $this->redirect($this->getRedirectPage('create', $model));
        }

        return $this->renderIsAjax('create', compact('model'));
    }
}
